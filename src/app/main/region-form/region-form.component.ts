import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RegionService } from 'src/app/service/region.service';

@Component({
  selector: 'app-region-form',
  templateUrl: './region-form.component.html',
  styleUrls: ['./region-form.component.css']
})
export class RegionFormComponent implements OnInit {
  myForm: FormGroup;
  zones: any[] = [
    {id: 1, name: 'North Region'},
    {id: 2, name: 'South Region'},
    {id: 3, name: 'West Region'},
    {id: 4, name: 'East Region'},
    {id: 5, name: 'Central Region'},
    {id: 6, name: 'Sabah Region'},
    {id: 7, name: 'Sarawak Region'},
  ];

  terminals: any[] = [
    {id: '1', name: 'Walkie Talkie'},
    {id: '2', name: 'RV'},
    {id: '3', name: 'Computer'},
  ];

  id: number = 0;
  terminalSaved: any[] = [];

  constructor(private regionService: RegionService, private route: Router,
    private activatedRoute: ActivatedRoute ) {
    this.myForm = new FormGroup({
      name: new FormControl('North Region Office'),
      description : new FormControl(),
      zone: new FormControl(7),
      terminal: new FormArray([]),
      active: new FormControl('N'),
    });
  }

  // on click edit, on click new
  ngOnInit(): void {
    let path = this.route.url;
    if (path !== "/region/new") {
      this.activatedRoute.paramMap.subscribe(params => {
        if(params.get('id')) {
          let id = Number(params.get('id'));
          console.log("id", this.id);
          this.id = id;
          this.getDetails(id);
        }
     });
    }
  }

  getDetails(id: number) {
    console.log("...", id);
    this.regionService.editMe(id).subscribe(data => {
      console.log(data);
      // set value dlm form control
      this.myForm.patchValue(data);
      this.terminalSaved = data.terminal;
      const checkArray: FormArray = this.myForm.get("terminal") as FormArray;
      for(let t of data.terminal) {
        checkArray.push(new FormControl('1'));
      }
      console.log(checkArray);
    });

  }

  onSubmit() {
    console.log(this.myForm.value);
    this.regionService.insert(this.myForm.value).subscribe(data => {
      console.log("respon", data);
    });
  }

  onChange(event: any) {
    console.log("any message", event.target.value);
    const checkArray: FormArray = this.myForm.get("terminal") as FormArray;

    if (event.target.checked) {
      // klik utk check
      checkArray.push(new FormControl(event.target.value));
    } else {
      // klik utk uncheck
      let i = 0;
      checkArray.controls.forEach(item => {
        if (item.value == event.target.value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  isChecked(id: number) {
    for(let terminal_id of this.terminalSaved) {
      if (id == terminal_id) {
        return true;
      }
    }
    return false;
  }

}
