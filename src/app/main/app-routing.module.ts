import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeFormComponent } from './employee-form/employee-form.component';
import { EmployeeComponent } from './employee/employee.component';
import { LayoutComponent } from './layout/layout.component';
import { OrgFormComponent } from './org-form/org-form.component';
import { OrgComponent } from './org/org.component';
import { RegionFormComponent } from './region-form/region-form.component';
import { RegionComponent } from './region/region.component';

const routes: Routes = [
  {path:'', redirectTo: "/main/employee/list", pathMatch: 'full'},
  {
    path: 'main', component: LayoutComponent, data: {title:"Employee"},
    children: [
      {path: 'employee/list', component: EmployeeComponent},
      {path: 'employee/new', component: EmployeeFormComponent},
      {path: 'employee/edit', component: EmployeeFormComponent},
      {path: 'employee/edit/:id', component: EmployeeFormComponent},
    ]
  },
  {
    path: 'region', component: LayoutComponent, data: {title:"Region"},
    children: [
      {path: 'list', component: RegionComponent},
      {path: 'new', component: RegionFormComponent},
      {path: 'edit/:id', component: RegionFormComponent},
    ]
  },
  {
    path: 'org', component: LayoutComponent, data: {title:"Branches"},
    children: [
      {path: 'list', component: OrgComponent},
      {path: 'new', component: OrgFormComponent},
      {path: 'edit', component: OrgFormComponent},
      {path: 'edit/:id', component: OrgFormComponent},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
