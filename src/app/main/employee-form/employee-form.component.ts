import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from 'src/app/service/employee.service';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.css']
})
export class EmployeeFormComponent implements OnInit {
  id: any = 0;
  first_name1 = '';
  last_name1 = '';
  email1 = '';

  constructor(private empSvc: EmployeeService, private route: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    let path = this.route.url;
    //console.log(path);
    if (path !== "/main/employee/new") {
      //edit, get details record
      let that = this;
      this.activatedRoute.paramMap.subscribe(params => {
        if(params.get('id')) {
          let id = Number(params.get('id'));
          console.log("id", this.id);
          that.getDetails(id);
        }
     });
    }
  }

  getDetails(id: number) {
    console.log("...", id);
    this.empSvc.editMe(id).subscribe(data => {
      console.log("...",data);
      this.first_name1 = data.first_name;
      this.last_name1 = data.last_name;
      this.email1 = data.email;
      this.id = data.id;
    });
  }

  onSave(myForm: NgForm) {
    console.log(myForm.value);

    if (this.id == 0) {
      this.empSvc.insert(myForm.value).subscribe(data => {
        console.log(data);
        this.route.navigateByUrl('/main/employee/list');
      });
    } else {
      this.empSvc.update(myForm.value, this.id).subscribe(data => {
        console.log(data);
        this.route.navigateByUrl('/main/employee/list');
      });
    }
  }

}
