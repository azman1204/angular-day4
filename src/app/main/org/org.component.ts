import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrgService } from 'src/app/service/org.service';

@Component({
  selector: 'app-org',
  templateUrl: './org.component.html',
  styleUrls: ['./org.component.css']
})
export class OrgComponent implements OnInit {
  orgs: any[] = [];

  constructor(private orgSvc: OrgService, private route: Router) { }

  ngOnInit(): void {
    this.orgSvc.getAll().subscribe(data => {
      console.log(data);
      this.orgs = data;
    });
  }

  listAll() {
    this.orgSvc.getAll().subscribe(data => {
      console.log(data);
      this.orgs = data;
    });
  }

  onEdit(id:number) {
    this.route.navigateByUrl('/org/edit/'+id);
  }

  onDelete(id:number) {
    if(confirm('Are you sure ?')) {
      this.orgSvc.delete(id).subscribe(data => {
        console.log(data);
        this.listAll();
      });
    }
  }
}
