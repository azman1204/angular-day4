import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from 'src/app/service/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  employees: any[] = [];
  no: number = 1;

  constructor(private empSvc: EmployeeService, private route: Router) { }

  ngOnInit(): void {
    this.listAll();
  }

  listAll() {
    this.empSvc.getAll().subscribe(data => {
      console.log(data);
      this.employees = data;
    });
  }

  onDelete(id: number) {
    if(confirm('Are you sure ?')) {
      this.empSvc.delete(id).subscribe(data => {
        console.log(data);
        this.listAll();
      });
    }
  }

  onEdit(id: number) {
    this.route.navigateByUrl('/main/employee/edit/'+id);
  }

}
