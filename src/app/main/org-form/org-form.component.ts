import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OrgService } from 'src/app/service/org.service';

@Component({
  selector: 'app-org-form',
  templateUrl: './org-form.component.html',
  styleUrls: ['./org-form.component.css']
})
export class OrgFormComponent implements OnInit {

  form: FormGroup;
  devices: any[] = [
    {id: 1, name: 'Device 1'},
    {id: 2, name: 'Device 2'},
    {id: 3, name: 'Device 3'}
  ];

  // devices saved on server
  devices2: any[] = [];
  zones: any[] = [
    {id: 1, name: 'Zone 1'},
    {id: 2, name: 'Zone 2'},
    {id: 3, name: 'Zone 3'}
  ];
  zone: string = '0';
  id:number = 0;


  constructor(private orgSvc: OrgService, private route: Router, private activatedRoute: ActivatedRoute) {
    this.form = new FormGroup({
      devices: new FormArray([]),
      name: new FormControl('John Doe'),
      description: new FormControl('...'),
      zone: new FormControl('0'),
      active: new FormControl('N'),
    });
  }

  // on show cehcboxes
  isChecked(id:number) {
    for(let device of this.devices2) {
      if (device == id) {
        return true;
      }
    }
    return false;
  }

  ngOnInit(): void {
    let path = this.route.url;
    if (path !== "/org/new") {
      //edit, get details record
      let that = this;
      this.activatedRoute.paramMap.subscribe(params => {
        if(params.get('id')) {
          let id = Number(params.get('id'));
          that.getDetails(id);
          that.id = id;
        }
     });
    }
  }

  // on edit
  getDetails(id: number) {
    console.log("...", id);
    this.orgSvc.editMe(id).subscribe(data => {
      console.log("...",data);
      this.devices2 = data.devices;
      this.form.patchValue({
        name: data.name,
        description: data.description,
        zone: data.zone,
        active: data.active
      });

      const checkArray: FormArray = this.form.get('devices') as FormArray;
      for(let d of data.devices) {
        checkArray.push(new FormControl(d));
      }
    });
  }

  // on click save button
  submitForm() {
    console.log(this.form.value);
    if (this.id == 0) {
      this.orgSvc.insert(this.form.value).subscribe(data => {
        console.log(data);
        this.route.navigateByUrl('/org/list');
      });
    } else {
      this.orgSvc.update(this.form.value, this.id).subscribe(data => {
        console.log(data);
        this.route.navigateByUrl('/org/list');
      });
    }
  }

  // on tick checkbox
  onCheckboxChange(e:any) {
    const checkArray: FormArray = this.form.get('devices') as FormArray;

    if (e.target.checked) {
      checkArray.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      checkArray.controls.forEach((item) => {
        if (item.value == e.target.value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
    console.log(this.form.value);
  }

}
