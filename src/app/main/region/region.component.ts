import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { RegionService } from 'src/app/service/region.service';

@Component({
  selector: 'app-region',
  templateUrl: './region.component.html',
  styleUrls: ['./region.component.css']
})
export class RegionComponent implements OnInit {
  regions: any[] = [];


  constructor(
    private regionService: RegionService,
    private route: Router
    ) { }

  ngOnInit(): void {
    this.regionService.getAll().subscribe(data => this.regions = data);
  }

  onEdit(id: number) {
    this.route.navigateByUrl('/region/edit/'+id);
  }

  onDelete(id: number) {

  }

}
