import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegionService {

  url = environment.url + '/region';

  constructor(private httpClient: HttpClient) { }

  // return all employee. async request
  getAll(): Observable<any> {
    return this.httpClient.get(this.url);
  }

  insert(data: any): Observable<any> {
    return this.httpClient.post(this.url, data);
  }

  delete(id: number): Observable<any> {
    const url = this.url + '/' + id;
    return this.httpClient.delete(url);
  }

  editMe(id: number): Observable<any> {
    const url = this.url + '/' + id;
    return this.httpClient.get(url);
  }

  update(data: any, id: number): Observable<any> {
    const url = this.url + '/' + id;
    return this.httpClient.patch(url, data);
  }
}
